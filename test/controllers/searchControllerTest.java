/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.Map;
import models.Question;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author bender
 */
public class searchControllerTest {
    
    public searchControllerTest() {
    }

    /**
     * Test of query method, of class searchController.
     */
    @Test
    public void testQueryEmptyString() {
        System.out.println("query empty string");
        String query = "";
        searchController instance = new searchController();
        ModelAndView expResult = new ModelAndView();
        expResult.addObject("query", query);
        expResult.addObject("questions", new Question[0]);
        expResult.setViewName("query");
        
        ModelAndView result = instance.query(query);
        Map<String, Object> resultModels = result.getModel();
        
        assertEquals(expResult.getViewName(), result.getViewName());
        assertTrue(resultModels.keySet().size() == 2);
        assertTrue(resultModels.containsKey("query"));
        assertTrue(resultModels.containsKey("questions"));
        assertEquals((String)resultModels.get("query"), query);
        assertEquals(resultModels.get("questions").getClass().getName(), expResult.getModel().get("questions").getClass().getName());
        assertTrue(((Question[])resultModels.get("questions")).length == 0);
    }
    
    /**
     * Test of query method, of class searchController.
     */
    @Test
    public void testQuerySomeWord() {
        System.out.println("query some word");
        String query = "test";
        searchController instance = new searchController();
        ModelAndView expResult = new ModelAndView();
        expResult.addObject("query", query);
        expResult.addObject("questions", new Question[30]);
        expResult.setViewName("query");
        
        ModelAndView result = instance.query(query);
        Map<String, Object> resultModels = result.getModel();
        
        assertEquals(expResult.getViewName(), result.getViewName());
        assertTrue(resultModels.keySet().size() == 2);
        assertTrue(resultModels.containsKey("query"));
        assertTrue(resultModels.containsKey("questions"));
        assertEquals((String)resultModels.get("query"), query);
        assertEquals(resultModels.get("questions").getClass().getName(), expResult.getModel().get("questions").getClass().getName());
        assertTrue(((Question[])resultModels.get("questions")).length == 30);
    }
    
    /**
     * Test of query method, of class searchController.
     */
    @Test
    public void testQuerySomeTextWithSpaces() {
        System.out.println("query with spaces");
        String query = "unit test";
        searchController instance = new searchController();
        ModelAndView expResult = new ModelAndView();
        expResult.addObject("query", query);
        expResult.addObject("questions", new Question[30]);
        expResult.setViewName("query");
        
        ModelAndView result = instance.query(query);
        Map<String, Object> resultModels = result.getModel();
        
        assertEquals(expResult.getViewName(), result.getViewName());
        assertTrue(resultModels.keySet().size() == 2);
        assertTrue(resultModels.containsKey("query"));
        assertTrue(resultModels.containsKey("questions"));
        assertEquals((String)resultModels.get("query"), query);
        assertEquals(resultModels.get("questions").getClass().getName(), expResult.getModel().get("questions").getClass().getName());
        assertTrue(((Question[])resultModels.get("questions")).length == 30);
    }
    
}
