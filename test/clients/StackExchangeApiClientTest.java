/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clients;

import models.Question;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bender
 */
public class StackExchangeApiClientTest {
    /**
     * Test of getQuestions method, of class StackExchangeApiClient. Trying to query it by some word.
     */
    @Test
    public void testGetQuestionsForWord() {
        System.out.println("getQuestions for word");
        CharSequence query = "test";
        StackExchangeApiClient instance = new StackExchangeApiClient();
        Question[] result = instance.getQuestionsWithTitleContains(query.toString());
        assertEquals(30, result.length);
        for(Question question: result)
        {
            assertTrue(question.getTitle().toLowerCase().contains(query));
        }
    }
    
    /**
     * Test of getQuestions method, of class StackExchangeApiClient. Trying to query it by some text with spaces.
     */
    @Test
    public void testGetQuestionsForTextWithSpaces() {
        System.out.println("getQuestions for text with spaces");
        String query = "unit test";
        CharSequence queryWord1 = "unit";
        CharSequence queryWord2 = "test";
        StackExchangeApiClient instance = new StackExchangeApiClient();
        Question[] result = instance.getQuestionsWithTitleContains(query);
        assertEquals(30, result.length);
        for(Question question: result)
        {
            assertTrue(question.getTitle().toLowerCase().contains(queryWord1) 
                        && question.getTitle().toLowerCase().contains(queryWord2));
        }
    }
    
    /**
     * Test of getQuestions method, of class StackExchangeApiClient. Trying to query it by empty string.
     */
    @Test
    public void testGetQuestionsForEmptyString() {
        System.out.println("getQuestions for empty string");
        String query = "";
        StackExchangeApiClient instance = new StackExchangeApiClient();
        Question[] result = instance.getQuestionsWithTitleContains(query);
        assertEquals(0, result.length);
    }
    
    /**
     * Test of getQuestions method, of class StackExchangeApiClient. Trying to query it by single white space.
     */
    @Test
    public void testGetQuestionsForWhiteSpace() {
        System.out.println("getQuestions for empty string");
        String query = " ";
        StackExchangeApiClient instance = new StackExchangeApiClient();
        Question[] result = instance.getQuestionsWithTitleContains(query);
        assertEquals(0, result.length);
    }
}
