<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/css/style.css" />" rel="stylesheet">
        <title>Type your question</title>
    </head>

    <body>
        <div  class="searchForm">
            <form action="/TestApplication/search/query.htm">
                <p class="hello"><i>Hello! Enter your query:</i></p>
                <input class="queryInput" type="text" name="query"/>
                <input class="submitButton" type="submit"/>
            </form>
        </div>
    </body>
</html>
