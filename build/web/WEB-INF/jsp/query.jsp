<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@page import="models.Question" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/css/style.css" />" rel="stylesheet">
        <title>Results</title>
    </head>

    <body>
        <p><b>This is what we get for you by query <i>${query}</i>:</b></p>
        <c:forEach items="${questions}" var="question" varStatus="loop">
        <div class="query${question.isAnswered ? ' answered' : ' unanswered'}${!loop.last ? '' : ' lastItem'}" >
            <div class="author">
                <c:if test="${not empty question.owner}">
                    <a href="${question.owner.link}">
                        <img src="${question.owner.profileImage}" class="picture" alt="${question.owner.displayName}"/>
                    </a>
                </c:if>
            </div>
            <div class="question">
                <a href="${question.link}" class="title">${question.title}${question.isAnswered ? ' [answered]' : ''}</a>
                <div class="tags">
                    <c:forEach items="${question.tags}" var="tag">
                        <a class="tag" href="query.htm?query=${tag}">${tag}</a>
                    </c:forEach>
                </div>
                <div>
                    <fmt:formatDate value="${question.creationDate}" var="formattedCreationDate" type="date" pattern="HH:mm:ss dd MMMM yyyy" />
                    <p class="timestamp">Started in: ${formattedCreationDate}</p>
                    <c:if test="${not empty question.lastEditDate}">
                        <fmt:formatDate value="${question.lastEditDate}" var="formattedEditDate" type="date" pattern="HH:mm:ss dd MMMM yyyy" />
                        <p class="timestamp">Last edit: ${formattedEditDate}</p>
                    </c:if>
                </div>
            </div>
        </div>
        </c:forEach>
    </body>
</html>
