/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Model represents the stack_exchange_site type in StackExchange API. Inclueds only public fields.
 * @author bender
 */
public class StackExchangeSite {
    private String[] aliases;
    private String apiSiteParameter;
    private String audience;
    private Date closedBetaDate;
    private String faviconUrl;
    private String highResolutionIconUrl;
    private String iconUrl;
    private Date launchDate;
    private String logoUrl;
    private String[] markdownExtentions;
    private String name;
    private Date openBetaDate;
    private RelatedSite[] relatedSites;
    private String siteState;
    private String siteType;
    private String siteUrl;
    private Styling styling;
    private String twitterAccount;
    
    public String[] getAliases()
    {
        return aliases;
    }
    public String getApiSiteParameter()
    {
        return apiSiteParameter;
    }
    public String getAudience()
    {
        return audience;
    }
    public Date getClosedBetaDate()
    {
        return closedBetaDate;
    }
    public String getFaviconUrl()
    {
        return faviconUrl;
    }
    public String getHighResolutionIconUrl()
    {
        return highResolutionIconUrl;
    }
    public String getIconUrl()
    {
        return iconUrl;
    }
    public Date getLaunchDate()
    {
        return launchDate;
    }
    public String getLogoUrl()
    {
        return logoUrl;
    }
    public String[] getMarkdownExtentions()
    {
        return markdownExtentions;
    }
    public String getName()
    {
        return name;
    }
    public Date getOpenBetaDate()
    {
        return openBetaDate;
    }
    public RelatedSite[] getRelatedSites()
    {
        return relatedSites;
    }
    public String getSiteState()
    {
        return siteState;
    }
    public String getSiteType()
    {
        return siteType;
    }
    public String getSiteUrl()
    {
        return siteUrl;
    }
    public Styling getStyling()
    {
        return styling;
    }
    public String getTwitterAccount()
    {
        return twitterAccount;
    }
    
    /**
     * Creates new instance of StackExchangeSite class from JSON document
     * @param info JSON document
     * @return new instance of the StackExchangeSite filled from JSON
     */
    public static StackExchangeSite getStackExchangeSite(JSONObject info)
    {
        StackExchangeSite site = new StackExchangeSite();
        site.apiSiteParameter = info.getString("api_site_parameter");
        site.audience = info.getString("audience");
        site.faviconUrl = info.getString("favicon_url");
        if(info.has("high_resolution_icon_url"))
        {
            site.highResolutionIconUrl = info.getString("high_resolution_icon_url");
        }
        site.iconUrl = info.getString("icon_url");
        site.logoUrl = info.getString("logo_url");
        site.name = info.getString("name");
        site.siteState = info.getString("siteState");
        site.siteType = info.getString("sute_type");
        site.siteUrl = info.getString("site_url");
        if(info.has("twitter_account"))
        {
            site.twitterAccount = info.getString("twitter_account");
        }
        if(info.has("closed_beta_date"))
        {
            site.closedBetaDate = new Date(info.getLong("closed_beta_date")*1000);
        }
        site.launchDate = new Date(info.getLong("launch_date")*1000);
        if(info.has("open_beta_date"))
        {
            site.openBetaDate = new Date(info.getLong("open_beta_date")*1000);
        }
        if(info.has("aliases"))
        {
            JSONArray jsAliases = info.getJSONArray("aliases");
            site.aliases = new String[jsAliases.length()];
            for(int i = 0; i < jsAliases.length(); i++)
            {
                site.aliases[i] = jsAliases.getString(i);
            }
        }
        if(info.has("markdown_extentions"))
        {
            JSONArray jsMarkdownExtentions = info.getJSONArray("markdown_extentions");
            site.markdownExtentions = new String[jsMarkdownExtentions.length()];
            for(int i = 0; i < jsMarkdownExtentions.length(); i++)
            {
                site.markdownExtentions[i] = jsMarkdownExtentions.getString(i);
            }
        }
        if(info.has("related_sites"))
        {
            JSONArray jsRelatedSites = info.getJSONArray("related_sites");
            site.relatedSites = new RelatedSite[jsRelatedSites.length()];
            for(int i = 0; i < jsRelatedSites.length(); i++)
            {
                site.relatedSites[i] = RelatedSite.getRelatedSite(jsRelatedSites.getJSONObject(i));
            }
        }
        site.styling = Styling.getStyling(info.getJSONObject("styling"));
        return site;
    }
}
