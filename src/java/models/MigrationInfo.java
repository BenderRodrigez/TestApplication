/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;
import org.json.JSONObject;

/**
 * Model represents the migration_info type in StackExchange API. Inclueds only public fields.
 * @author bender
 */
public class MigrationInfo {
    private Date onDate;
    private StackExchangeSite otherSite;
    private int questionId;
    
    private MigrationInfo()
    {
        
    }
    
    
    public Date getOnDate()
    {
        return onDate;
    }
    public StackExchangeSite getOtherSite()
    {
        return otherSite;
    }
    public int getQuestionId()
    {
        return questionId;
    }
    
    /**
     * Creates new instance of MigrationInfo class from JSON document
     * @param info JSON document
     * @return new instance of the Question filled by JSON data
     */
    public static MigrationInfo getMigrationInfo(JSONObject info)
    {
        MigrationInfo migrationInfo = new MigrationInfo();
        migrationInfo.questionId = info.getInt("question_id");
        migrationInfo.onDate = new Date(info.getLong("on_date")*1000);
        migrationInfo.otherSite = StackExchangeSite.getStackExchangeSite(info.getJSONObject("other_site"));
        return migrationInfo;
    }
}
