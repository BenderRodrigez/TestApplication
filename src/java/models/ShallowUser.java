/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import org.json.JSONObject;

/**
 * Model for shallow_usesr type in StackExchange API. Inclueds only public fields.
 * @author bender
 */
public class ShallowUser {
    private int acceptRate;
    private String displayName;
    private String link;
    private String profileImage;
    private int reputation;
    private int userId;
    private String userType;
    
    public int getAcceptRate()
    {
        return acceptRate;
    }
    public String getDisplayName()
    {
        return displayName;
    }
    public String getLink()
    {
        return link;
    }
    public String getProfileImage()
    {
        return profileImage;
    }
    public int getReputation()
    {
        return reputation;
    }
    public int getUserId()
    {
        return userId;
    }
    public String getUserType()
    {
        return userType;
    }
    
    /**
     * Creates new instance of ShallowUser class from JSON document
     * @param info JSON whith contains the shallow_user info from StackExchange API
     * @return new instance of ShallowUser
     */
    public static ShallowUser getShallowUser(JSONObject info)
    {
        ShallowUser user = new ShallowUser();
        if(info.has("accept_rate"))
        {
            user.acceptRate = info.getInt("accept_rate");
        }
        if(info.has("display_name"))
        {
            user.displayName = info.getString("display_name");
        }
        if(info.has("link"))
        {
            user.link = info.getString("link");
        }
        if(info.has("profile_image"))
        {
            user.profileImage = info.getString("profile_image");
        }
        if(info.has("reputation"))
        {
            user.reputation = info.getInt("reputation");
        }
        if(info.has("user_id"))
        {
            user.userId = info.getInt("user_id");
        }
        if(info.has("user_type"))
        {
            user.userType = info.getString("user_type");
        }
        return user;
    }
}
