/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import org.json.JSONObject;

/**
 * Model represents the related_site type in StackExchange API. Inclueds only public fields.
 * @author bender
 */
public class RelatedSite {
    private String apiSiteParameter;
    private String name;
    private String relation;
    private String siteUrl;
    
    
    public String getApiSiteParameter()
    {
        return apiSiteParameter;
    }
    public String getName()
    {
        return name;
    }
    public String getRelation()
    {
        return relation;
    }
    public String getSiteUrl()
    {
        return siteUrl;
    }
    
    /**
     * Creates new instance of RelatedSite class from JSON document
     * @param info
     * @return new instance of the RelatedSite filled from JSON
     */
    public static RelatedSite getRelatedSite(JSONObject info)
    {
        RelatedSite relatedSite = new RelatedSite();
        relatedSite.apiSiteParameter = info.getString("api_site_parameter");
        relatedSite.name = info.getString("name");
        relatedSite.relation = info.getString("relation");
        relatedSite.siteUrl = info.getString("site_url");
        return relatedSite;
    }
}
