/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import org.json.JSONObject;

/**
 * Model represents the styling type in StackExchange API. Inclueds only public fields.
 * @author bender
 */
public class Styling {
    private String linkColor;
    private String tagBackgroundColor;
    private String tagForegroundColor;
    
    private Styling()
    {
    }
    
    public String getLinkColor()
    {
        return linkColor;
    }
    public String getTagBackgroundColor()
    {
        return tagBackgroundColor;
    }
    public String getTagForegroundColor()
    {
        return tagForegroundColor;
    }
    
    /**
     * Creates new instance of Styling class from JSON document
     * @param info JSON document
     * @return new instance of the StackExchangeSite filled from JSON
     */
    public static Styling getStyling(JSONObject info)
    {
        Styling styling = new Styling();
        styling.linkColor = info.getString("link_color");
        styling.tagBackgroundColor = info.getString("tag_background_color");
        styling.tagForegroundColor = info.getString("tag_foreground_color");
        return styling;
    }
}
