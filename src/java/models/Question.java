/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Model represents the question type in StackExchange API. Inclueds only public fields.
 * @author bender
 */
public class Question {
    private int acceptedAnswerId;
    private int answerCount;
    private int bountyAmount;
    private Date bountyClosesDate;
    private Date closedDate;
    private String closedReason;
    private Date communityOwnedDate;
    private Date creationDate;
    private Boolean isAnswered;
    private Date lastActivityDate;
    private Date lastEditDate;
    private String link;
    private Date lockedDate;
    private MigrationInfo migratedFrom;
    private MigrationInfo migratedTo;
    private ShallowUser owner;
    private Date protectedDate;
    private int questionId;
    private int score;
    private String[] tags;
    private String title;
    private int viewCount;
    
    public int getAcceptedAnswerId()
    {
        return acceptedAnswerId;
    }
    public int getAnswerCount()
    {
        return answerCount;
    }
    public int getBountyAmount()
    {
        return bountyAmount;
    }
    public Date getBountyClosesDate()
    {
        return bountyClosesDate;
    }
    public Date getClosedDate()
    {
        return closedDate;
    }
    public String getClosedReason()
    {
        return closedReason;
    }
    public Date getCommunityOwnedDate()
    {
        return communityOwnedDate;
    }
    public Date getCreationDate()
    {
        return creationDate;
    }
    public Boolean getIsAnswered()
    {
        return isAnswered;
    }
    public Date getLastActivityDate()
    {
        return lastActivityDate;
    }
    public Date getLastEditDate()
    {
        return lastEditDate;
    }
    public String getLink()
    {
        return link;
    }
    public Date getLockedDate()
    {
        return lockedDate;
    }
    public MigrationInfo getMigratedFrom()
    {
        return migratedFrom;
    }
    public MigrationInfo getMigratedTo()
    {
        return migratedTo;
    }
    public ShallowUser getOwner()
    {
        return owner;
    }
    public Date getProtectedDate()
    {
        return protectedDate;
    }
    public int getQuestionId()
    {
        return questionId;
    }
    public int getScore()
    {
        return score;
    }
    public String[] getTags()
    {
        return tags;
    }
    public String getTitle()
    {
        return title;
    }
    public int getViewCount()
    {
        return viewCount;
    }

    /**
     * Creates new instance of Question class from JSON document
     * @param info JSON document
     * @return new instance of the Question filled by JSON data
     */
    public static Question getQuestion(JSONObject info)
    {
        Question currentPost = new Question();
        if(info.has("accept_answer_id"))
        {
            currentPost.acceptedAnswerId = info.getInt("accepted_answer_id");
        }
        currentPost.answerCount = info.getInt("answer_count");
        if(info.has("bounty_amount"))
        {
            currentPost.bountyAmount = info.getInt("bounty_amount");
        }
        if(info.has("bounty_closes_date"))
        {
            currentPost.bountyClosesDate = new Date(info.getLong("bounty_closes_date")*1000);
        }
        if(info.has("closed_date"))
        {
            currentPost.closedDate = new Date(info.getLong("closed_date")*1000);
        }
        if(info.has("closed_reason"))
        {
            currentPost.closedReason = info.getString("closed_reason");
        }
        if(info.has("community_owned_date"))
        {
            currentPost.communityOwnedDate = new Date(info.getLong("community_owned_date")*1000);
        }
        currentPost.creationDate = new Date(info.getLong("creation_date")*1000);
        currentPost.isAnswered = info.getBoolean("is_answered");
        currentPost.lastActivityDate = new Date(info.getLong("last_activity_date")*1000);
        if(info.has("last_edit_date"))
        {
            currentPost.lastEditDate = new Date(info.getLong("last_edit_date")*1000);
        }
        currentPost.link = info.getString("link");
        if(info.has("locked_date"))
        {
            currentPost.lockedDate = new Date(info.getLong("locked_date")*1000);
        }
        if(info.has("migrated_from"))
        {
            currentPost.migratedFrom = MigrationInfo.getMigrationInfo(info.getJSONObject("migrated_from"));
        }
        if(info.has("migrated_to"))
        {
            currentPost.migratedTo = MigrationInfo.getMigrationInfo(info.getJSONObject("migrated_to"));
        }
        if(info.has("owner"))
        {
            currentPost.owner = ShallowUser.getShallowUser(info.getJSONObject("owner"));
        }
        if(info.has("protected_date"))
        {
            currentPost.protectedDate = new Date(info.getLong("protected_date")*1000);
        }
        currentPost.questionId = info.getInt("question_id");
        currentPost.score = info.getInt("score");
        currentPost.title = info.getString("title");
        currentPost.viewCount = info.getInt("view_count");
        
        JSONArray jsTags = info.getJSONArray("tags");
        currentPost.tags = new String[jsTags.length()];
        for(int i = 0; i < jsTags.length(); i++)
        {
            currentPost.tags[i] = jsTags.getString(i);
        }
        return currentPost;
    }
}
