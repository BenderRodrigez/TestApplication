/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import models.Question;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Client for Stack Exchange Public API implements StackExchangeApiClientInterface
 * @author bender
 */
public class StackExchangeApiClient implements StackExchangeApiClientInterface {
    @Override
    public Question[] getQuestionsWithTitleContains(String query)
    {
        if(query.isEmpty() || " ".equals(query))
        {
            return new Question[0];
        }
        try {
            HttpURLConnection conn = createConnection(query);
            String output = handleResponse(conn);
            Question[] questions = parseFromJSON(output);
            return questions;
        }
        catch (JSONException | IOException ex) {
            Logger.getLogger(StackExchangeApiClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Question[0];
    }
    
    /**
     * Parse JSON document which contains an array of the questions as described in StackExchange API
     * @param response JSON document body
     * @return Array of Questions
     */
    private Question[] parseFromJSON(String response)
    {
        JSONObject jsonResponse = new JSONObject(response);
        JSONArray jsonQuestions = jsonResponse.getJSONArray("items");
        Question[] questions = new Question[jsonQuestions.length()];
        for (int i = 0; i < jsonQuestions.length(); i++) {
            JSONObject currentItem = jsonQuestions.getJSONObject(i);
            Question currentPost = Question.getQuestion(currentItem);
            questions[i] = currentPost;
        }
        return questions;
    }

    /**
     * Read response from HTTP connection
     * @param connection An HTTP connection to the API server
     * @return response data as single string
     * @throws IOException 
     */
    private String handleResponse(HttpURLConnection connection) throws IOException {
        if (connection.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + connection.getResponseCode());
        }
        //assume data compressed by GZIP algorithm
        BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(connection.getInputStream())));
        String response = br.lines().reduce((s, t) -> s + t).orElse("");
        connection.disconnect();
        return response;
    }

    /**
     * Initiates HTTP connection to the Stack Exchange API
     * @param query user's query string
     * @return HTTP connection to the api server
     * @throws MalformedURLException
     * @throws IOException
     * @throws ProtocolException
     */
    private HttpURLConnection createConnection(String query) throws MalformedURLException, IOException, ProtocolException {
        URL url = new URL(BASE_API_URL+query.replace(' ', '+'));
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");
        return connection;
    }
}
