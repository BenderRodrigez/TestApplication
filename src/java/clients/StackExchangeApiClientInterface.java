/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clients;

import models.Question;

/**
 * StackExchange API client interface. Describes basic operations with API.
 * @author bender
 */
public interface StackExchangeApiClientInterface {

    /**
     * Base URI of API queries
     */
    final String BASE_API_URL = "http://api.stackexchange.com/2.2/search?order=desc&sort=activity&site=stackoverflow&intitle=";
    
    /**
     * Send request to StackExchange API and return an array of the search results
     * @param query user's query string
     * @return array of the Questions matched by search
     */
    Question[] getQuestionsWithTitleContains(String query);
    
}
