/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import clients.StackExchangeApiClient;
import models.Question;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Base controller in the application
 * @author bender
 */
@Controller
public class searchController {
    
    /**
     * 
     */
    public searchController()
    {
        
    }
    
    /**
     * Provides search result view and get data for it
     * @param query user's search query string
     * @return query view
     */
    @RequestMapping(value = "/query.htm", method = RequestMethod.GET)
    public ModelAndView query(String query)
    {
        StackExchangeApiClient client = new StackExchangeApiClient();
        Question[] questions = client.getQuestionsWithTitleContains(query);
        
        ModelAndView mv = new ModelAndView();
        mv.addObject("query", query);
        mv.addObject("questions", questions);
        mv.setViewName("query");
        return mv;
    }
}
